package com.murakaa.politizeapp.utils

import android.content.Context
import com.murakaa.politizeapp.data.remote.FeedListDataSource
import com.murakaa.politizeapp.ui.feed.ViewModelFactory

/**
 *
 *  Singleton helper for injection purposes.
 *
 *  Main goal is to pass the model data to viewmodel
 *
 */
object InjectorUtils {
    fun provideFeedViewModelFactory( context: Context): ViewModelFactory {
        val model = FeedListDataSource.getInstance(context)
        return ViewModelFactory(model)
    }


}