package com.murakaa.politizeapp.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * Helper methods for Date and String conversion
 *
 * @todo should prepare for internationalization on function getFormattedDate()
 */

fun getDateFromString(dateString: String): Date {
    val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
    return inputFormat.parse(dateString)
}

fun getFormattedDate(date: Date): String {
    //should prepare for internationalization
    //    val locale = Locale.getDefault()
    val locale = Locale("pt", "BR")

    val outputFormat = SimpleDateFormat("dd 'de' MMMM 'de' yyyy", locale)
    return outputFormat.format(date)
}