package com.murakaa.politizeapp.ui.feed


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.murakaa.politizeapp.data.remote.IFeedListDataSource



class ViewModelFactory(private val dataSource: IFeedListDataSource)
    : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FeedViewModel(dataSource) as T
    }
}