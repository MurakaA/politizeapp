package com.murakaa.politizeapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.murakaa.politizeapp.R
import com.murakaa.politizeapp.ui.feed.FeedFragment

class FeedActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.feed_activity)
        //setting toolbar
        setSupportActionBar(findViewById(R.id.toolbar))

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, FeedFragment.newInstance())
                .commitNow()
        }
    }

}
