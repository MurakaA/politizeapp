package com.murakaa.politizeapp.ui.feed

import androidx.lifecycle.ViewModel
import com.murakaa.politizeapp.data.remote.IFeedListDataSource

/**
 * Feed Fragment View model.
 *
 * Contains data to be observed by the view and triggers fetching data from server, using models interface (IFeedListDataSource)
 *
 */
class FeedViewModel (private val feedListDataSource: IFeedListDataSource) : ViewModel() {
    //Connectivity Helpers
    val connectionOffline = feedListDataSource.connectionOffline
    val connectionOK = feedListDataSource.connectionOK
    val connectionFailed = feedListDataSource.connectionFailed

    // reference a LiveData with a String
     val feedItems = feedListDataSource.downloadedFeedList

    suspend fun getFeedList() =  feedListDataSource.fetchFeedList()

}
