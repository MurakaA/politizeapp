package com.murakaa.politizeapp.ui.feed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.airbnb.lottie.LottieAnimationView
import com.murakaa.politizeapp.R
import com.murakaa.politizeapp.adapters.FeedAdapter
import com.murakaa.politizeapp.data.FeedItem
import com.murakaa.politizeapp.utils.InjectorUtils
import com.murakaa.politizeapp.utils.SpacesItemDecoration
import kotlinx.android.synthetic.main.feed_fragment.view.*
import kotlinx.android.synthetic.main.layout_request_states.view.*
import kotlinx.coroutines.*

/**
 * Fragment responsible for presenting the Feed list data
 */
class FeedFragment : Fragment()
{

    private lateinit var job: Job

    companion object {
        fun newInstance() = FeedFragment()
    }

    //UI components
    private lateinit var viewModel: FeedViewModel
    private lateinit var rcvFeedList: RecyclerView
    private lateinit var loadingAnimation: LottieAnimationView
    private lateinit var offlineAnimation: LottieAnimationView
    private lateinit var onErrorAnimation: LottieAnimationView
    private lateinit var onErrorTextView: TextView
    private lateinit var swipeToRefreshView: SwipeRefreshLayout

    //custom adapter
    private lateinit var feedAdapter: FeedAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.feed_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    /**
     * Initialize UI Components references
     */
    private fun initUI() {
        rcvFeedList = this.view!!.rootView.rcv_feed_frag
        loadingAnimation = this.view!!.rootView.lottie_loading
        loadingAnimation.playAnimation()
        loadingAnimation.visibility = View.VISIBLE
        onErrorAnimation = this.view!!.rootView.lottie_on_error
        offlineAnimation = this.view!!.rootView.lottie_offline
        onErrorTextView = this.view!!.rootView.tv_feed_on_error
        swipeToRefreshView = this.view!!.rootView.swipe_refresh_feed_frag
        initRecyclerView()
        swipeToRefreshView.setOnRefreshListener {
            startJob()
        }

    }

    private fun startJob() {
        //
        //trigger viewmodel fetch data (not using Global scope
        job = CoroutineScope(Dispatchers.IO).launch {
            onItemsLoadComplete()
            val result = viewModel.getFeedList()
            withContext(Dispatchers.Main) {
                when (result) {
                    viewModel.connectionOffline -> showOfflineState()
                    viewModel.connectionOK -> readyScreenForData()
                    viewModel.connectionFailed -> showErrorState()
                }
            }
        }
        job.start()
    }


    private fun onItemsLoadComplete() {
        //Stop refreshing
        swipeToRefreshView.isRefreshing = false
    }

    private fun initRecyclerView() {
        rcvFeedList.layoutManager = LinearLayoutManager(this.context)
        rcvFeedList.addItemDecoration(
            SpacesItemDecoration(30)
        )
    }

    private fun setUpRecyclerView(itemsList: List<FeedItem>) {
        feedAdapter = FeedAdapter(itemsList)
        rcvFeedList.adapter = feedAdapter

        feedAdapter.onReadMoreClick = { selectedItem ->
            //requirement(3) - Button Behaviour
            Toast.makeText(this.context, selectedItem.title, Toast.LENGTH_LONG).show()
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        connectVM()
    }

    private fun connectVM() {
        // inject dependency
        val factory = InjectorUtils.provideFeedViewModelFactory(this.context!!)
        viewModel = ViewModelProviders.of(this, factory).get(FeedViewModel::class.java)

        //view observes the LiveData Object
        viewModel.feedItems.observe(this, Observer { list ->
            //create the observable to the LiveData
            loadingAnimation.visibility = View.GONE
            if (list.isNotEmpty()) {
                rcvFeedList.visibility = View.VISIBLE
                setUpRecyclerView(list)
            } else {
                showListEmptyState()
            }
        })
        startJob()
    }

    private fun showListEmptyState() {
        Toast.makeText(this.context, "Empty list STate", Toast.LENGTH_LONG).show()
    }

    private fun showErrorState() {
        offlineAnimation.visibility = View.GONE
        loadingAnimation.visibility = View.GONE
        onErrorAnimation.visibility = View.VISIBLE
        onErrorTextView.visibility = View.VISIBLE
        onErrorAnimation.playAnimation()
        offlineAnimation.pauseAnimation()
        rcvFeedList.visibility = View.GONE
    }

    private fun readyScreenForData() {
        loadingAnimation.visibility = View.GONE
        offlineAnimation.visibility = View.GONE
        onErrorAnimation.visibility = View.GONE
        onErrorTextView.visibility = View.GONE
        rcvFeedList.visibility = View.VISIBLE
        offlineAnimation.pauseAnimation()
        onErrorAnimation.pauseAnimation()


    }

    private fun showOfflineState() {
        rcvFeedList.visibility = View.GONE
        loadingAnimation.visibility = View.GONE
        offlineAnimation.visibility = View.VISIBLE
        offlineAnimation.playAnimation()
        onErrorAnimation.visibility = View.GONE
        onErrorTextView.visibility = View.GONE
        Toast.makeText(this.context, getString(R.string.error_msg_offline), Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        job.cancel()
        super.onDestroy()
    }

}
