package com.murakaa.politizeapp.data.remote

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.murakaa.politizeapp.data.FeedItem
import com.murakaa.politizeapp.data.remote.network.ConnectivityInterceptor
import com.murakaa.politizeapp.data.remote.network.IApiRetrofit
import com.murakaa.politizeapp.data.remote.network.response.FeedListResponse
import com.murakaa.politizeapp.utils.getDateFromString
import com.murakaa.politizeapp.utils.getFormattedDate
import java.io.IOException
import java.lang.Exception

/**
 *
 * This class is responsible to implement the model manipulation/update methods
 * Abstracting these from the View and ViewModel
 *
 */
class FeedListDataSource (private val apiService: IApiRetrofit): IFeedListDataSource {



    private val _downloadedFeedList = MutableLiveData<List<FeedItem>>()

    //live data is not mutable, needs to be casted
    override val downloadedFeedList: LiveData<List<FeedItem>>
        get() = _downloadedFeedList //To change initializer of created properties use File | Settings | File Templates.

    /***
     * Method that triggers api server request
     */
    override suspend fun fetchFeedList() : String {
        try {
            val fetchFeedList = apiService.getAllFeedItemsAsync().await()
            _downloadedFeedList.postValue(convertToListItem(fetchFeedList))
        } catch (e: IOException) {
            Log.e("Connectivity Error -> ", "No internet - $e")
            return connectionOffline
        } catch (e: Exception) {
            Log.e("Error fetching data", "$e")
            return connectionFailed
        }
        return connectionOK
    }

    //Converter from response to simple object
    private fun convertToListItem(fetchFeedList: FeedListResponse): List<FeedItem>? {
        val feedItemsList: MutableList<FeedItem> = mutableListOf()

        for (doc in fetchFeedList.documents!!.iterator()) {
            val publishedDate = getDateFromString(doc!!.fields!!.publishedAt!!.timestampValue!!)
            val formattedPublishedDate = getFormattedDate(publishedDate)
            val item = FeedItem(
                doc.fields?.title?.title,
                doc.name,
                doc.fields?.summary?.stringValue,
                doc.fields?.publishedAt?.timestampValue,
                doc.fields?.thumbnail?.stringValue,
                doc.createTime,
                doc.updateTime,
                publishedDate,
                formattedPublishedDate
            )
            feedItemsList.add(item)
        }

        //requirement(1) - List sorting - descending by published date
        feedItemsList.sortByDescending {
            it.publishedDate
        }
        return feedItemsList.toList()
    }

    //Creating instance to access model
    companion object {
        //Adding volatile makes sure that the value of variable is refreshed.
        @Volatile
        private var instance: FeedListDataSource? = null
        fun getInstance(context: Context) : FeedListDataSource{
            val retrofit = IApiRetrofit(ConnectivityInterceptor(context))

            instance ?: synchronized(this) {
                instance ?: FeedListDataSource(retrofit).also { instance = it }
            }
            return instance!!
        }


    }
}