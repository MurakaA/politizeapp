package com.murakaa.politizeapp.data.remote.network.response

import com.google.gson.annotations.SerializedName

/**
 * Data class for Feed list Server response
 */

data class FeedListResponse(
    @SerializedName("documents")
    val documents: List<Document?>?
)

data class Document (
    @SerializedName("createTime")
    val createTime: String?,
    @SerializedName("fields")
    val fields: Fields?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("updateTime")
    val updateTime: String?
)

data class Fields(
    @SerializedName("published_at")
    val publishedAt: PublishedAt?,
    @SerializedName("summary")
    val summary: Summary?,
    @SerializedName("thumbnail")
    val thumbnail: Thumbnail?,
    @SerializedName("title")
    val title: Title?
)

data class PublishedAt(
    @SerializedName("timestampValue")
    val timestampValue: String?
)

data class Title(
    @SerializedName("stringValue")
     val title: String
)

data class Thumbnail(
    @SerializedName("stringValue")
    val stringValue: String?
)

data class Summary(
    @SerializedName("stringValue")
    val stringValue: String?
)