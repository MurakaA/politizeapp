package com.murakaa.politizeapp.data.remote.network

import okhttp3.Interceptor

/**
 * Connectivity Interceptor Interface
 */
interface IConnectivityInterceptor : Interceptor
