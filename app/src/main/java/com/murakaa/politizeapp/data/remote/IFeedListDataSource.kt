package com.murakaa.politizeapp.data.remote

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.murakaa.politizeapp.data.FeedItem


/***
 * Main entry point for accessing the Feed data from server
 * It uses LiveData as the observable data holder class
 * This class is responsible to implement the model manipulation methods provide the entry point of ViewModels to the Model components
 */
interface IFeedListDataSource {
    //Connectivity Helpers
     val connectionOffline: String
        get() = "connectionOffline"
    val connectionOK: String
        get() = "connectionOK"
    val connectionFailed: String
        get() = "connectionFailed"

    val downloadedFeedList: LiveData<List<FeedItem>>

    /**
     * Method responsible to fetch feed list data from server
     * Suspend functions enable their execution to be suspended if needed
     */
    suspend fun fetchFeedList(): String

}