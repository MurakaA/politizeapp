package com.murakaa.politizeapp.data.remote.network

/**
 * Helper Class to hold Endpoint specific constants
 *
 */
 class APIConstants {
    companion object {
        const val BASE_URL = "https://firestore.clients6.google.com/v1beta1/projects/eduk-free/databases/(default)/"

        //feed Endpoint
        const val FEED_DOCUMENTS_ENDPOINT = "documents/politize-feed"

    }
}