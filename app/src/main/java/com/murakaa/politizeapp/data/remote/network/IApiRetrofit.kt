package com.murakaa.politizeapp.data.remote.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.murakaa.politizeapp.data.remote.network.response.FeedListResponse
import retrofit2.http.GET
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Class responsible to define service parameters and requests
 * Using Retrofit
 */
interface IApiRetrofit {

    @GET(APIConstants.FEED_DOCUMENTS_ENDPOINT)
    fun getAllFeedItemsAsync() : Deferred<FeedListResponse>

    //like static methods
    companion object {
        // deals with offline fetching
        // using the operator enables methods overload Objects (with invoke() method this class can be invoked as a function).
        operator fun invoke(
            connectivityInterceptor: IConnectivityInterceptor
        ): IApiRetrofit {

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(connectivityInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(APIConstants.BASE_URL)
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(IApiRetrofit::class.java)
        }
    }
}