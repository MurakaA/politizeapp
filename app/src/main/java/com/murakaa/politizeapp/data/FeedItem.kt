package com.murakaa.politizeapp.data

import java.util.*

/**
 * Data class that holds the Feed item's information from server.
 * Contains formatted data
 */
data class FeedItem(
     val title: String?,
     val name: String?,
     val summary: String?,
     val publishedAt: String?,
     val thumbnailURL: String?,
     val createTime: String?,
     val updateTime: String?,
     val publishedDate: Date?,
     val formattedPublishedDate: String?
)
