package com.murakaa.politizeapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.murakaa.politizeapp.R
import com.murakaa.politizeapp.data.FeedItem
import kotlinx.android.synthetic.main.feed_list_item.view.*

class FeedItemViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.feed_list_item, parent, false)) {
    
    private var mNameInfo: String? = null

    init {
        mNameInfo = ""
    }

    fun bind(feedListItem: FeedItem, onItemClick: ((FeedItem) -> Unit)?) {
        itemView.tv_feed_item_title.text = feedListItem.title
//        mTitleView?.text = feedListItem.title

        itemView.tv_feed_item_published_at.text = feedListItem.formattedPublishedDate
        itemView.tv_feed_item_summary.text = feedListItem.summary
        mNameInfo = feedListItem.name

        if(feedListItem.thumbnailURL!=null && feedListItem.thumbnailURL.isNotEmpty()){
            itemView.tv_feed_item_empty_thumbnail_placeholder.visibility= View.GONE
            itemView.iv_feed_item_thumbnail.visibility=View.VISIBLE
            Glide.with(this.itemView.context)
                .load(feedListItem.thumbnailURL)
                .apply(RequestOptions().override(1024, 1024))
                .into(itemView.iv_feed_item_thumbnail)
        }else{
            itemView.iv_feed_item_thumbnail.visibility=View.GONE
            itemView.tv_feed_item_empty_thumbnail_placeholder.visibility= View.VISIBLE
            itemView.tv_feed_item_empty_thumbnail_placeholder.text = feedListItem.title
        }

        itemView.btn_feed_item_reed_more.setOnClickListener {
            onItemClick!!.invoke(feedListItem)
        }
    }

}