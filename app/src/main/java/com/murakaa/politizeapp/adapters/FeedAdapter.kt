package com.murakaa.politizeapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.murakaa.politizeapp.data.FeedItem

class FeedAdapter(private var items: List<FeedItem> ) : RecyclerView.Adapter<FeedItemViewHolder>() {
    //OnClickListener for the button, using lambda
    var onReadMoreClick: ((FeedItem) -> Unit)? = null
    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedItemViewHolder {
        val view = LayoutInflater.from(parent.context)
        return FeedItemViewHolder(view, parent)
    }

    override fun onBindViewHolder(holder: FeedItemViewHolder, position: Int) {
        val feedItem = items[position]
        holder.bind(feedItem, onReadMoreClick)
    }

}

