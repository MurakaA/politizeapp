# PolitizeApp

**Eduk's Android developer's challenge**


This App renders the Politize! feed list, ordering the posts be descending published date.
By Aline Murakami



**Technologies**

Chosen Architecture - MVVM
    
Language: Kotlin
    
    Libraries of note:
    
    1.  Glide - Image rendering
    2.  Retrofit - HTTP client
    3.  Lottie - simple and fast animations
    4.  Kotlin Android Coroutines - Thread safe operations
    
    
